
package com.abocode.codemaker.database;

import lombok.Data;

@Data
public class Column {
    public static final String OPTION_REQUIRED = "required:true";
    public static final String OPTION_NUMBER_INSEX = "precision:2,groupSeparator:','";
    private String fieldDbName;
    private String fieldName;
    private String filedComment;
    private String fieldType;
    private String classType;
    private String classType_row;
    private String optionType;
    private String charmaxLength;
    private String precision;
    private String scale;
    private String nullable;
}
