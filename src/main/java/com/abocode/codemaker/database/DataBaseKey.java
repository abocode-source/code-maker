package com.abocode.codemaker.database;


public interface DataBaseKey{
 String UUID = "uuid";
 String IDENTITY = "identity";
 String SEQUENCE = "sequence";
}
