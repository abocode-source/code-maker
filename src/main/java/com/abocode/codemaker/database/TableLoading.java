package com.abocode.codemaker.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import com.abocode.codemaker.util.DataBaseUtils;
import com.abocode.codemaker.util.ResourceUtils;
import com.abocode.codemaker.util.StringUtils;

import static com.abocode.codemaker.database.DBConstant.*;

public class TableLoading {
    private Connection conn;
    private Statement stmt;
    private String sql;
    private ResultSet rs;

    public TableLoading() {
    }
    public List<String> readAllTableNames() throws SQLException {
        ArrayList tableNames = new ArrayList(0);

        try {
            Class.forName(ResourceUtils.DIVER_NAME);
            this.conn = DriverManager.getConnection(ResourceUtils.URL, ResourceUtils.USERNAME, ResourceUtils.PASSWORD);
            this.stmt = this.conn.createStatement(1005, 1007);
            if(ResourceUtils.DATABASE_TYPE.equals(DBConstant.DATABASE_TYPE_MYSQL)) {
                this.sql = MessageFormat.format(DBConstant.MYSQL_DB_SQL_QUERYNAME, new Object[]{DataBaseUtils.getV(ResourceUtils.DATABASE_NAME)});
            }

            if(ResourceUtils.DATABASE_TYPE.equals(DBConstant.DATABASE_TYPE_ORACLE)) {
                this.sql = ORACLE_DB_SQL_QUERYNAME;
            }

            if(ResourceUtils.DATABASE_TYPE.equals(DBConstant.DATABASE_TYPE_POSTGRESQL)) {
                this.sql = POSTGRESQL_DB_SQL_QUERYNAME;
            }

            if(ResourceUtils.DATABASE_TYPE.equals(DBConstant.DATABASE_TYPE_SQL_SERVER)) {
                this.sql =SQLSERVER_DB_SQL_QUERYNAME;
            }
            this.rs = this.stmt.executeQuery(this.sql);
            while(this.rs.next()) {
                String e = this.rs.getString(1);
                tableNames.add(e);
            }
        } catch (Exception var11) {
            var11.printStackTrace();
        } finally {
            DataBaseUtils.close(stmt);
            DataBaseUtils.conn(conn);
        }
        return tableNames;
    }

    public List<Column> readTableColumn(String tableName) throws Exception {
        ArrayList columntList = new ArrayList();

        Column ch;
        try {
            Class.forName(ResourceUtils.DIVER_NAME);
            this.conn = DriverManager.getConnection(ResourceUtils.URL, ResourceUtils.USERNAME, ResourceUtils.PASSWORD);
            this.stmt = this.conn.createStatement(1005, 1007);
            if(ResourceUtils.DATABASE_TYPE.equals(DBConstant.DATABASE_TYPE_MYSQL)) {
                this.sql = MessageFormat.format(MYSQL_DB_SQL, new Object[]{DataBaseUtils.getV(tableName.toUpperCase()), DataBaseUtils.getV(ResourceUtils.DATABASE_NAME)});

            }

            if(ResourceUtils.DATABASE_TYPE.equals(DBConstant.DATABASE_TYPE_ORACLE)) {
                this.sql = MessageFormat.format(ORACLE_DB_SQL, new Object[]{DataBaseUtils.getV(tableName.toUpperCase())});
            }

            if(ResourceUtils.DATABASE_TYPE.equals(DBConstant.DATABASE_TYPE_POSTGRESQL)) {
                this.sql = MessageFormat.format(POSTGRESQL_DB_SQL, new Object[]{DataBaseUtils.getV(tableName.toLowerCase())});
            }

            if(ResourceUtils.DATABASE_TYPE.equals(DBConstant.DATABASE_TYPE_SQL_SERVER)) {
                this.sql = MessageFormat.format(SQLSERVER_DB_SQL, new Object[]{DataBaseUtils.getV(tableName.toLowerCase())});
            }

            this.rs = this.stmt.executeQuery(this.sql);
            this.rs.last();
            int rsList = this.rs.getRow();
            if(rsList <= 0) {
                throw new Exception("该表不存在或者表中没有字段");
            }

            ch = new Column();
            if(ResourceUtils.FILED_CONVERT) {
                ch.setFieldName(formatField(this.rs.getString(1).toLowerCase()));
            } else {
                ch.setFieldName(this.rs.getString(1).toLowerCase());
            }

            ch.setFieldDbName(this.rs.getString(1).toUpperCase());
            ch.setFieldType(formatField(this.rs.getString(2).toLowerCase()));
            ch.setPrecision(this.rs.getString(4));
            ch.setScale(this.rs.getString(5));
            ch.setCharmaxLength(this.rs.getString(6));
            ch.setNullable(DataBaseUtils.getNullAble(this.rs.getString(7)));
            this.formatFieldClassType(ch);
            ch.setFiledComment(org.apache.commons.lang.StringUtils.isBlank(this.rs.getString(3))?ch.getFieldName():this.rs.getString(3));
            String[] ui_filter_fields = new String[0];
            if(ResourceUtils.GENERATE_UI_FILTER_FIELDS != null) {
                ui_filter_fields = ResourceUtils.GENERATE_UI_FILTER_FIELDS.toLowerCase().split(",");
            }

            if(!ResourceUtils.GENERATE_TABLE_ID.equals(ch.getFieldName()) && !StringUtils.isIn(ch.getFieldDbName().toLowerCase(), ui_filter_fields)) {
                columntList.add(ch);
            }

            while(this.rs.previous()) {
                Column po = new Column();
                if(ResourceUtils.FILED_CONVERT) {
                    po.setFieldName(formatField(this.rs.getString(1).toLowerCase()));
                } else {
                    po.setFieldName(this.rs.getString(1).toLowerCase());
                }

                po.setFieldDbName(this.rs.getString(1).toUpperCase());
                if(!ResourceUtils.GENERATE_TABLE_ID.equals(po.getFieldName()) && !StringUtils.isIn(po.getFieldDbName().toLowerCase(), ui_filter_fields)) {
                    po.setFieldType(formatField(this.rs.getString(2).toLowerCase()));
                    po.setPrecision(this.rs.getString(4));
                    po.setScale(this.rs.getString(5));
                    po.setCharmaxLength(this.rs.getString(6));
                    po.setNullable(DataBaseUtils.getNullAble(this.rs.getString(7)));
                    this.formatFieldClassType(po);
                    po.setFiledComment(org.apache.commons.lang.StringUtils.isBlank(this.rs.getString(3))?po.getFieldName():this.rs.getString(3));
                    columntList.add(po);
                }
            }
        } catch (ClassNotFoundException var16) {
            throw var16;
        } catch (SQLException var17) {
            throw var17;
        } finally {
            DataBaseUtils.close(stmt);
            DataBaseUtils.conn(conn);

        }

        ArrayList var19 = new ArrayList();

        for(int i = columntList.size() - 1; i >= 0; --i) {
            ch = (Column)columntList.get(i);
            var19.add(ch);
        }

        return var19;
    }

    public List<Column> readOriginalTableColumn(String tableName) throws Exception {
        ArrayList columntList = new ArrayList();

        Column ch;
        try {
            Class.forName(ResourceUtils.DIVER_NAME);
            this.conn = DriverManager.getConnection(ResourceUtils.URL, ResourceUtils.USERNAME, ResourceUtils.PASSWORD);
            this.stmt = this.conn.createStatement(1005, 1007);
            if(ResourceUtils.DATABASE_TYPE.equals(DBConstant.DATABASE_TYPE_MYSQL)) {
                this.sql = MessageFormat.format(MYSQL_DB_SQL, new Object[]{DataBaseUtils.getV(tableName.toUpperCase()), DataBaseUtils.getV(ResourceUtils.DATABASE_NAME)});
            }

            if(ResourceUtils.DATABASE_TYPE.equals(DBConstant.DATABASE_TYPE_ORACLE)) {
                this.sql = MessageFormat.format(ORACLE_DB_SQL, new Object[]{DataBaseUtils.getV(tableName.toUpperCase())});
            }

            if(ResourceUtils.DATABASE_TYPE.equals(DBConstant.DATABASE_TYPE_POSTGRESQL)) {
                this.sql = MessageFormat.format(POSTGRESQL_DB_SQL, new Object[]{DataBaseUtils.getV(tableName.toLowerCase())});
            }

            if(ResourceUtils.DATABASE_TYPE.equals(DBConstant.DATABASE_TYPE_SQL_SERVER)) {
                this.sql = MessageFormat.format(SQLSERVER_DB_SQL, new Object[]{DataBaseUtils.getV(tableName.toLowerCase())});
            }

            this.rs = this.stmt.executeQuery(this.sql);
            this.rs.last();
            int rsList = this.rs.getRow();
            if(rsList <= 0) {
                throw new Exception("该表不存在或者表中没有字段");
            }

            ch = new Column();
            if(ResourceUtils.FILED_CONVERT) {
                ch.setFieldName(formatField(this.rs.getString(1).toLowerCase()));
            } else {
                ch.setFieldName(this.rs.getString(1).toLowerCase());
            }

            ch.setFieldDbName(this.rs.getString(1).toUpperCase());
            ch.setPrecision(DataBaseUtils.getNullString(this.rs.getString(4)));
            ch.setScale(DataBaseUtils.getNullString(this.rs.getString(5)));
            ch.setCharmaxLength(DataBaseUtils.getNullString(this.rs.getString(6)));
            ch.setNullable(DataBaseUtils.getNullAble(this.rs.getString(7)));
            ch.setFieldType(this.formatDataType(this.rs.getString(2).toLowerCase(), ch.getPrecision(), ch.getScale()));
            this.formatFieldClassType(ch);
            ch.setFiledComment(org.apache.commons.lang.StringUtils.isBlank(this.rs.getString(3))?ch.getFieldName():this.rs.getString(3));
            columntList.add(ch);

            while(this.rs.previous()) {
                Column po = new Column();
                if(ResourceUtils.FILED_CONVERT) {
                    po.setFieldName(formatField(this.rs.getString(1).toLowerCase()));
                } else {
                    po.setFieldName(this.rs.getString(1).toLowerCase());
                }

                po.setFieldDbName(this.rs.getString(1).toUpperCase());
                po.setPrecision(DataBaseUtils.getNullString(this.rs.getString(4)));
                po.setScale(DataBaseUtils.getNullString(this.rs.getString(5)));
                po.setCharmaxLength(DataBaseUtils.getNullString(this.rs.getString(6)));
                po.setNullable(DataBaseUtils.getNullAble(this.rs.getString(7)));
                po.setFieldType(this.formatDataType(this.rs.getString(2).toLowerCase(), po.getPrecision(), po.getScale()));
                this.formatFieldClassType(po);
                po.setFiledComment(org.apache.commons.lang.StringUtils.isBlank(this.rs.getString(3))?po.getFieldName():this.rs.getString(3));
                columntList.add(po);
            }
        } catch (ClassNotFoundException var15) {
            throw var15;
        } catch (SQLException var16) {
            throw var16;
        } finally {
            DataBaseUtils.close(stmt);
            DataBaseUtils.conn(conn);
        }

        ArrayList var18 = new ArrayList();

        for(int i = columntList.size() - 1; i >= 0; --i) {
            ch = (Column)columntList.get(i);
            var18.add(ch);
        }

        return var18;
    }

    public static String formatField(String field) {
        String[] strs = field.split("_");
        field = "";
        int m = 0;

        for(int length = strs.length; m < length; ++m) {
            if(m > 0) {
                String tempStr = strs[m].toLowerCase();
                tempStr = tempStr.substring(0, 1).toUpperCase() + tempStr.substring(1, tempStr.length());
                field = field + tempStr;
            } else {
                field = field + strs[m].toLowerCase();
            }
        }

        return field;
    }


    public boolean checkTableExist(String tableName) {
        try {
            System.out.println("数据库驱动: " + ResourceUtils.DIVER_NAME);
            Class.forName(ResourceUtils.DIVER_NAME);
            this.conn = DriverManager.getConnection(ResourceUtils.URL, ResourceUtils.USERNAME, ResourceUtils.PASSWORD);
            this.stmt = this.conn.createStatement(1005, 1007);
            if(ResourceUtils.DATABASE_TYPE.equals(DBConstant.DATABASE_TYPE_MYSQL)) {
                this.sql = MessageFormat.format(MYSQL_DB_SQL, new Object[]{DataBaseUtils.getV(tableName.toUpperCase()), DataBaseUtils.getV(ResourceUtils.DATABASE_NAME)});
            }

            if(ResourceUtils.DATABASE_TYPE.equals(DBConstant.DATABASE_TYPE_ORACLE)) {
                this.sql = MessageFormat.format(ORACLE_DB_SQL, new Object[]{DataBaseUtils.getV(tableName.toUpperCase())});
            }

            if(ResourceUtils.DATABASE_TYPE.equals(DBConstant.DATABASE_TYPE_POSTGRESQL)) {
                this.sql = MessageFormat.format(POSTGRESQL_DB_SQL, new Object[]{DataBaseUtils.getV(tableName.toLowerCase())});
            }

            if(ResourceUtils.DATABASE_TYPE.equals(DBConstant.DATABASE_TYPE_SQL_SERVER)) {
                this.sql = MessageFormat.format(SQLSERVER_DB_SQL, new Object[]{DataBaseUtils.getV(tableName.toLowerCase())});
            }

            this.rs = this.stmt.executeQuery(this.sql);
            this.rs.last();
            int e = this.rs.getRow();
            return e > 0;
        } catch (Exception var3) {
            var3.printStackTrace();
            return false;
        }
    }

    private void formatFieldClassType(Column columnt) {
        String fieldType = columnt.getFieldType();
        String scale = columnt.getScale();
        columnt.setClassType("inputxt");
        if("N".equals(columnt.getNullable())) {
            columnt.setOptionType("*");
        }

        if(!"datetime".equals(fieldType) && !fieldType.contains("time")) {
            if("date".equals(fieldType)) {
                columnt.setClassType("easyui-datebox");
            } else if(fieldType.contains("int")) {
                columnt.setOptionType("n");
            } else if("number".equals(fieldType)) {
                if(org.apache.commons.lang.StringUtils.isNotBlank(scale) && Integer.parseInt(scale) > 0) {
                    columnt.setOptionType("d");
                }
            } else if(!"float".equals(fieldType) && !"double".equals(fieldType) && !"decimal".equals(fieldType)) {
                if("numeric".equals(fieldType)) {
                    columnt.setOptionType("d");
                }
            } else {
                columnt.setOptionType("d");
            }
        } else {
            columnt.setClassType("easyui-datetimebox");
        }

    }

    private String formatDataType(String dataType, String precision, String scale) {
        if(dataType.contains("char")) {
            dataType = "java.lang.String";
        } else if(dataType.contains("int")) {
            dataType = "java.lang.Integer";
        } else if(dataType.contains("float")) {
            dataType = "java.lang.Float";
        } else if(dataType.contains("double")) {
            dataType = "java.lang.Double";
        } else if(dataType.contains("number")) {
            if(org.apache.commons.lang.StringUtils.isNotBlank(scale) && Integer.parseInt(scale) > 0) {
                dataType = "java.math.BigDecimal";
            } else if(org.apache.commons.lang.StringUtils.isNotBlank(precision) && Integer.parseInt(precision) > 10) {
                dataType = "java.lang.Long";
            } else {
                dataType = "java.lang.Integer";
            }
        } else if(dataType.contains("decimal")) {
            dataType = "BigDecimal";
        } else if(dataType.contains("date")) {
            dataType = "java.util.Date";
        } else if(dataType.contains("time")) {
            dataType = "java.util.Date";
        } else if(dataType.contains("blob")) {
            dataType = "byte[]";
        } else if(dataType.contains("clob")) {
            dataType = "java.sql.Clob";
        } else if(dataType.contains("numeric")) {
            dataType = "BigDecimal";
        } else {
            dataType = "java.lang.Object";
        }

        return dataType;
    }
}
