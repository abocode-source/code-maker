package com.abocode.codemaker.util;

import org.apache.commons.lang.StringUtils;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class DataBaseUtils {
    public static String getNullAble(String nullable){
        if("YES".equals(nullable) || "yes".equals(nullable) || "y".equals(nullable) || "Y".equals(nullable) || "f".equals(nullable))
            return "Y";
        if("NO".equals(nullable) || "N".equals(nullable) || "no".equals(nullable) || "n".equals(nullable) || "t".equals(nullable))
            return "N";
        else
            return null;
    }

    public static String getNullString(String nullable)
    {
        if(StringUtils.isBlank(nullable))
            return "";
        else
            return nullable;
    }

    public static String getV(String s)
    {
        return (new StringBuilder("'")).append(s).append("'").toString();
    }

    public static void close(Statement stmt) throws SQLException {
        if(stmt != null) {
            stmt.close();
            stmt = null;
            System.gc();
        }
    }

    public static void conn(Connection conn) throws SQLException {
        if(conn != null) {
            conn.close();
            conn = null;
            System.gc();
        }
    }
}
