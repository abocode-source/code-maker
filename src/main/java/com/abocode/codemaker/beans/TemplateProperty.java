package com.abocode.codemaker.beans;


import lombok.Data;

@Data
public class TemplateProperty{
    private boolean actionFlag;
    private boolean serviceFlag;
    private boolean entityFlag;
    private boolean pageFlag;
    private boolean serviceImplFlag;
    private boolean jspFlag;
    private String jspMode;
}
