package java;

import com.abocode.codemaker.database.Column;
import com.abocode.codemaker.database.TableLoading;
import org.apache.commons.lang.ArrayUtils;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

/**
 * Description:
 * @author: guanxianfei
 * @date: 2019/7/26
 */
public class TableLoadingTest {
    public static void main(String[] args) throws SQLException {
        try {
            List e = (new TableLoading()).readTableColumn("person");
            Iterator var3 = e.iterator();

            while(var3.hasNext()) {
                Column c = (Column)var3.next();
                System.out.println(c.getFieldName());
            }
        } catch (Exception var4) {
            var4.printStackTrace();
        }

        System.out.println(ArrayUtils.toString((new TableLoading()).readAllTableNames()));
    }
}
