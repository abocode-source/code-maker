#code-maker 说明
 code-maker是为JFaster提供的完善的代码生成器

##功能说明

    1.所有代码均开源，开源协议详见LICENSE
    2.使用方法，运行：com.abocode.codemaker.Run即可
    3.项目配置文件修改：contextConfig.properties
    4.数据库连接修改：database.properties
    5.模版修改：maker-config/template/*

##共享开源：

   欢迎各位开发者提出建议、push代码,如有疑问联系，请使用如下联系方式。邮箱:guanxf@aliyun.com,昵称:糊涂,交流qq群:140586555。